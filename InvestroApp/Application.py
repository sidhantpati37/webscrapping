"""
The script helps to take input from user and update user details on database. The script also facilitates to
select an image file from the FileChooser DialogBox and attest a profile picture. The items include,

# ANNUAL_INCOME
# GENDER
# SERVICE
# AADHAR_NO
# PAN_NO
# ADDRESS
# PIN_CODE
"""

from WebScrapping import *

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.slider import Slider
from kivy.graphics import *
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.filechooser import FileChooserIconView, FileChooserListView
from kivy.uix.popup import Popup
from kivy.uix.checkbox import CheckBox
from kivy.base import runTouchApp
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.uix.dropdown import DropDown
from kivy.uix.image import Image
import numpy as np
import pandas as pd
from dfguik import DfguiWidget


class BackLayout(FloatLayout):
    """
    The class helps to create the background layout of the main screen.
    Creates a Red rectangular Floatlayout, which automatically re-sizes
    and re-positions itself based on screen size.
    """

    def __init__(self, **kwargs):
        super(BackLayout, self).__init__(**kwargs) # **kwargs to inherit various parameters of Floatlayout

        # canvas.before helps to insert colour around the rectangular background
        # and update it based on screen size.
        with self.canvas.before:
            Color(1, 0, 0, .1, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle

        # Event binding refers to activating triggers based on execution of some event
        # As in this case, as the size of window increases or decreases the red background
        # rectangle will undergo change in position and size to fit the window
        self.bind(size=self._update_rect, pos=self._update_rect)

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


class salary_slider(GridLayout):
    """
    The slider helps user to choose annual salary using slider to mark the appropriate value.
    """

    def __init__(self, **kwargs): # self is the object of salary_slider class
        super(salary_slider, self).__init__(**kwargs) # **kwargs to inherit various parameters of Floatlayout

        # # canvas helps to highlight the corresponding area of GridLayout on the main screen.
        # # This can be used only to visualise the corresponding area on the screen.
        # with self.canvas:
        #     Color(0, 1, 0, 1, mode="rgba")  # Add green color
        #     self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        # self.bind(size=self._update_rect, pos=self._update_rect)

        self.rows = 1
        self.id = "salary"
        self.add_widget(Label(text="Annual Income"))

        self.salary = Slider(min=0, max=100,
                             value_track=True,
                             value_track_color=[0, 1, 0, 1])  # Green colour
        self.add_widget(self.salary)

        self.salaryvalue = Label(text="Rs 0.00")
        self.add_widget(self.salaryvalue)

        self.salary.bind(value=self.on_value_change)

    def on_value_change(self, instance, value):
        # value returns the decimal number of salary selected
        self.salaryvalue.text = "Rs. " + str(np.round(value, 2)) + " Lakhs"
        # SidhantPati().ANNUAL_INCOME = np.round(value, 2)

        presen_val_serv = App.get_running_app().SERVICE

        object = SidhantPati()

        object.SERVICE = presen_val_serv
        object.ANNUAL_INCOME = np.round(value, 2)

    # def _update_rect(self, instance, value):
    #     self.rect.pos = instance.pos
    #     self.rect.size = instance.size


class GenderCheckBox(GridLayout):
    """
    It prompts the user to check the appropriate gender give in the checkbox.
    The CheckBoxes have been grouped to give them the function of toggle keys or
    radio buttons and hence only one of them can be selected at a moment.
    """
    def __init__(self, **kwargs):
        super(GenderCheckBox, self).__init__(**kwargs)

        self.cols = 7

        self.add_widget(Label(text="Gender :"))

        self.add_widget(Label(text="Male"))
        self.male = CheckBox(active=True, color=[0, 1, 0, 1], group="sex")
        self.add_widget(self.male)

        self.add_widget(Label(text="Female"))
        self.female = CheckBox(active=True, color=[0, 1, 0, 1], group="sex")
        self.add_widget(self.female)

        self.add_widget(Label(text="NA"))
        self.na = CheckBox(active=True, color=[0, 1, 0, 1], group="sex")
        self.add_widget(self.na)


class DropdownList(FloatLayout):
    """
    The Dropdown presents a list of employement type to choose from.
    One can select the appropriate service based on one's employment.
    """
    def __init__(self, **kwargs):
        super(DropdownList, self).__init__(**kwargs)

        options = ["Public Sector", "Private Sector", "Self-Employed", "Agriculture"]
        drop_position = {'center_x': .425, 'center_y': .6}

        self.add_widget(Label(text="Service", font_size="15sp",
                              pos_hint={'center_x': drop_position["center_x"] - .08,
                                        'center_y': drop_position["center_y"]}))
        # Object of DropDown() created
        self.dropdown = DropDown()
        # if 'self.dropdown' is replaced by 'dropdown', then the dropwdown doesnot function,
        # this is mainly because of the garbage collection. The 'dropdown' reference in
        # __init__() method is not saved (The bind uses a weakref which will not prevent garbage collection).
        # So it is better to replace 'dropdown' local variable with a 'self.dropdown' instance variable.
        # https://stackoverflow.com/questions/56577526/kivy-dropdown-not-opening-in-screenmanager-but-open-in-screen


        # For each of the 4 option, a custom button is created which is thereafter binded to dropdown
        for opt in options:
            btn = Button(text=opt, size_hint_y=None, height=44)
            btn.bind(on_release=lambda val: self.dropdown.select(val.text))
            self.dropdown.add_widget(btn)

        # A primary/main button is created with the instance of dropdown activated on it
        self.mainButton = Button(text="---SELECT---", size_hint=(.1, .07), pos_hint=drop_position)
        self.mainButton.bind(on_release=self.dropdown.open)

        self.dropdown.bind(on_select=self.select_options)
        self.add_widget(self.mainButton)

    def select_options(self, instance, x):
        self.mainButton.text = x

        # To retrieve the current active value of the salary slider
        presen_val_sal = App.get_running_app().ANNUAL_INCOME

        object = SidhantPati() # Object of main class is created

        # The object of main class is used to update both the values simoltaneously by their current values,
        # so that none of the values gets missed to be updated
        object.SERVICE = x
        object.ANNUAL_INCOME = presen_val_sal


class TextInputBox(GridLayout):
    """
    The 4 textboxes prompt the user to input appropriate personal details and
    press of the submit button for the values to get updated.
    """

    def __init__(self, **kwargs):
        super(TextInputBox, self).__init__(**kwargs)

        self.cols = 2
        self.row_force_default = True
        self.row_default_height = 60
        self.spacing = [0, 20]

        add_label = Label(text="Address", size_hint_x=None, width=100)
        self.add_text = TextInput(text="--Enter Address Here---", multiline=True)
        pin_label = Label(text="Pin Code", size_hint_x=None, width=100)
        self.pin = TextInput(text="--Enter Pin Code---", multiline=False)
        aadhar_label = Label(text="Aadhar No.", size_hint_x=None, width=100)
        self.aadhar_text = TextInput(text="--Enter Aadhar Here---", multiline=False)
        pan_label = Label(text="Pan No.", size_hint_x=None, width=100)
        self.pan_text = TextInput(text="--Enter PAN Here---", multiline=False)
        self.submit = Button(text="Submit",
                             font_size="20sp",
                             color=[0, 1, 0, 1],
                             size_hint_x=None, width=100,
                             on_press=self.on_enter,
                             on_release=self.final_submit)

        self.add_widget(aadhar_label)
        self.add_widget(self.aadhar_text)
        self.add_widget(pan_label)
        self.add_widget(self.pan_text)
        self.add_widget(add_label)
        self.add_widget(self.add_text)
        self.add_widget(pin_label)
        self.add_widget(self.pin)
        self.add_widget(self.submit)
        self.add_widget(Label(text="I hereby confirm that all the details submitted by me are correct \n"
                                   "and to the best of my knowledge."))

    def on_enter(self, instance):
        global ADDRESS
        ADDRESS = self.add_text.text

        global PIN_CODE
        PIN_CODE = self.pin.text

        global AADHAR_NO
        AADHAR_NO = self.aadhar_text.text

        global PAN_NO
        PAN_NO = self.pan_text.text

    def final_submit(self, instance):
        """
        A popup has been created to showcase the success message
        :param instance:
        :return:
        """
        FinalMsgLayout = GridLayout(cols=1)
        msgLabel = Label(text="SUCESSFUL!!!\nYour data has been uploaded successfully.")
        ok_button = Button(text="Ok", font_size="20sp", color=[0, 1, 0, 1], size_hint_y=None, height=35)
        FinalMsgLayout.add_widget(msgLabel)
        FinalMsgLayout.add_widget(ok_button)

        final_popup = Popup(title="Submit",
                            content=FinalMsgLayout,
                            size_hint=(.3, .2),
                            pos_hint={'center_x': .5, 'center_y': .5})
        final_popup.open()
        ok_button.bind(on_press=final_popup.dismiss)

        """
        https://stackoverflow.com/questions/62272041/how-to-get-value-outside-function-using-python-and-kivy

        Retrieve the current values across all the widgets on the screen.
        4 textbox inputs can be directly accessed from the above function.
        In irder to retreve the current value of salary and service, 
        an object of running app has been created to access the appropriate values.
        """

        current_instance = App.get_running_app()
        SALARY = current_instance.ANNUAL_INCOME
        SERV = current_instance.SERVICE

        print(SERV)
        print(SALARY)
        print(ADDRESS)
        print(PIN_CODE)
        print(AADHAR_NO)
        print(PAN_NO)


class imageFilechooser(BoxLayout):
    """
    On the press of upload button, a filechooser dialogbox pops up
    with a hierarchy of the filesystem on this machine and
    hence using this GUI we can select the image that we want to upload.
    """
    def __init__(self, **kwargs):
        super(imageFilechooser, self).__init__(**kwargs)

        self.orientation = "vertical"
        # self.fileview = FileChooserIconView(size_hint_y=0.8) # Icon view
        self.fileview = FileChooserListView(size_hint_y=0.8) # List view
        self.add_widget(self.fileview)

        # Controls layout provide us with 2 buttons namely close and upload file and
        # a label to display te complete file path of the selected file.
        controls = GridLayout(cols=5, row_force_default=True, row_default_height=35, size_hint_y=0.14)
        self.file_name = Button(text="Close", size_hint_x=None, width=150)
        self.file_path = Label(text="File path is...", font_size="20sp")
        self.file_select = Button(text="Upload File", size_hint_x=None, width=150)

        self.file_select.bind(on_press=self.on_enter)
        self.fileview.bind(selection=self.on_mouse_select)

        controls.add_widget(self.file_name)
        controls.add_widget(self.file_path)
        controls.add_widget(self.file_select)

        self.add_widget(controls)

    def on_enter(self, obj):
        # Clicking on upload file button, will transfer the absolute path of the file selected
        self.file_path.text = str(self.fileview.selection[0])
        SelectedFilePath = str(self.fileview.selection[0])

        # Once the appropriate file is selected, the corresponding image is added to the layout
        layout_6 = FloatLayout(size_hint=(.3, .5))
        pic_uploaded = uploadedImage(uploaded_path=SelectedFilePath,
                                     pos_hint={'center_x': 2.5, 'center_y': 1.05})
        layout_6.add_widget(pic_uploaded)

        # The layout containing the selected image is dynamically added to the mainlayout,
        # by creating a current/running instance of the app.
        App.get_running_app().root.get_screen("main").add_widget(layout_6)
        print("Pic uploaded succesfully!!!!")

    def on_mouse_select(self, obj, val): # 'obj' is same as 'insatance'
        self.file_path.text = str(self.fileview.selection[0])

    def on_touch_up(self, touch):
        if self.fileview.selection:
            self.file_path.text = str(self.fileview.selection[0])
        return super().on_touch_up(touch)


class uploadedImage(BoxLayout):
    """
    The seleceted image is pasted on red background.
    The final layout is being accessed by imageFilechooser -> on_enter -> pic_uploaded.
    """
    def __init__(self, uploaded_path, **kwargs):
        super(uploadedImage, self).__init__(**kwargs)

        with self.canvas:
            Color(1, 0, 0, .1, mode="rgba")
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        self.bind(size=self._update_rect, pos=self._update_rect)

        finalImage = Image(source=uploaded_path)
        self.add_widget(finalImage)

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


class Header(GridLayout):
    def __init__(self, **kwargs):
        super(Header, self).__init__(**kwargs)

        with self.canvas.before:
            Color(0, 0, 0, 1, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        self.bind(size=self._update_rect, pos=self._update_rect)

        cred = BoxLayout(orientation="vertical", size_hint_x=None, width=75)
        cred.add_widget(Label(text="Name : Sidhant Pati"))
        cred.add_widget(Label(text="DOB : 08/08/1993"))
        cred.add_widget(Label(text="Occupation : Service"))
        cred.add_widget(Label(text="State : Odisha"))

        self.rows = 1
        self.add_widget(Image(source='Sidhant Amazon.jpg', size_hint_x=None, width=150))
        self.add_widget(cred)
        self.add_widget(Label(text="INVESTRO", font_size="35sp", size_hint_x=None, width=960))
        self.signout = Button(text="Sign-Out", size_hint_x=None, width=220)
        self.add_widget(self.signout)


    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


class Login(BoxLayout):
    def __init__(self, **kwargs):
        super(Login, self).__init__(**kwargs)

        with self.canvas.before:
            Color(1, 0, 0, .3, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        self.bind(size=self._update_rect, pos=self._update_rect)

        self.orientation = "vertical"
        self.padding = [10, 5]

        self.add_widget(Label(text="Sign-In", font_size="30sp"))
        self.add_widget(Label(text="Username", size_hint_x=0))
        self.username = TextInput(text="", multiline=False)
        self.add_widget(self.username)
        self.add_widget(Label(text="Password", size_hint_x=0))
        self.password = TextInput(text="", multiline=False, password=True)
        self.add_widget(self.password)
        self.add_widget(Label(text=""))
        self.continue_button = Button(text="Continue", font_size="20sp")
        self.add_widget(self.continue_button)

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


class InvestmentOptions(GridLayout):
    def __init__(self, **kwargs):
        super(InvestmentOptions, self).__init__(**kwargs)

        with self.canvas.before:
            Color(0, 0, 0, 1, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        self.bind(size=self._update_rect, pos=self._update_rect)

        self.cols = 2

        self.spacing = [0, 40]

        self.add_widget(Label(text="Post Office Schemes"))
        self.add_widget(Button(text="Download info for PO Schemes", on_release=self.investSchemes))

        self.add_widget(Label(text="ELSS Schemes"))
        self.add_widget(Button(text="Download info for ELSS Schemes", on_release=self.investSchemes))

        self.add_widget(Label(text="NBFC FD Schemes"))
        self.add_widget(Button(text="Download info for NBFC FD Schemes", on_release=self.investSchemes))

        self.add_widget(Label(text="Bank FD Schemes"))
        self.add_widget(Button(text="Download info for Bank FD Schemes", on_release=self.investSchemes))

        self.add_widget(Label(text="Bank RD Schemes"))
        self.add_widget(Button(text="Download info for Bank RD Schemes", on_release=self.investSchemes))

    def investSchemes(self, instance):
        if instance.text == "Download info for PO Schemes":
            Postoffice_savingschemes()
        elif instance.text == "Download info for ELSS Schemes":
            elss()
        elif instance.text == "Download info for NBFC FD Schemes":
            nbfc_fd_interestrates()
        elif instance.text == "Download info for Bank FD Schemes":
            bank_fd_interestrates()
        else:
            bank_rd_interestrates()

        self.successDownloadLayout = GridLayout(cols=1)
        self.successLabel = Label(text="Congratualtions!!!\nYour file has been downloaded successfully.")
        ok_button = Button(text="Ok", font_size="20sp", color=[0, 1, 0, 1], size_hint_y=None, height=35)
        self.successDownloadLayout.add_widget(self.successLabel)
        self.successDownloadLayout.add_widget(ok_button)

        success_popup = Popup(title="Success",
                            content=self.successDownloadLayout,
                            size_hint=(.3, .2),
                            pos_hint={'center_x': .5, 'center_y': .5})
        success_popup.open()
        ok_button.bind(on_press=success_popup.dismiss)



    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size

class InvestmentInfo(BoxLayout):
    def __init__(self, **kwargs):
        super(InvestmentInfo, self).__init__(**kwargs)

        with self.canvas.before:
            Color(0, 0, 0, 1, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        self.bind(size=self._update_rect, pos=self._update_rect)

        df = saving_schemes()
        self.add_widget(DfguiWidget(df))

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size

class loginScreen(Screen):
    def __init__(self, **kwargs):
        super(loginScreen, self).__init__(**kwargs)

        self.layout = layout = BackLayout()

        self.log = Login(size_hint=(.25, .4), pos_hint={'center_x': .5, 'center_y': .5})
        self.log.continue_button.bind(on_press=self.signin)
        layout.add_widget(self.log)

        layout.add_widget(Label(text="The application is developed and owned by SIDHANT PATI."
                                     " Any usage has to be approved by the owner. All rights reserved.",
                                font_size="12sp",
                                pos_hint={'center_x': .5, 'center_y': .01}))

        self.add_widget(layout)

    def signin(self, instance):
        print(self.log.username.text)
        print(self.log.password.text)
        log_name = self.log.username.text
        log_pass = self.log.password.text

        if log_name=="sidhant" and log_pass=="sidhant":
            self.manager.current="main"
        else:
            wrong_cred = GridLayout(cols=1)
            wrongLabel = Label(text="Incorrect credentials. Please try again")
            ok_button = Button(text="Ok", font_size="20sp", color=[0, 1, 0, 1], size_hint_y=None, height=35)
            wrong_cred.add_widget(wrongLabel)
            wrong_cred.add_widget(ok_button)

            wrong_popup = Popup(title="Submit",
                                content=wrong_cred,
                                size_hint=(.3, .2),
                                pos_hint={'center_x': .5, 'center_y': .5})
            wrong_popup.open()
            ok_button.bind(on_press=wrong_popup.dismiss)


class mainScreen(Screen):
    def __init__(self, **kwargs):
        super(mainScreen, self).__init__(**kwargs)

        self.layout = layout = BackLayout()


        """#################################### Left Label (User Info) ####################################"""

        head = Header(size_hint=(1, .1), pos_hint={'center_x': .5, 'center_y': .925})
        head.signout.bind(on_release=self.on_signout)
        layout.add_widget(head)


        """#################################### Added Coloured Box & Msg Box ####################################"""
        with layout.canvas.before:
            Color(1, 0, 0, .5, mode="rgba")  # Add a red color
            Rectangle(pos=(750, 100), size=(650, 135))  # Add a rectangle

        sub_layout = FloatLayout()
        self.msg_label = Label(text="No Clicks yet!!!",
                               font_size="35sp",
                               size_hint=(.5, .7),
                               pos_hint={'center_x': .75, 'center_y': .185})
        sub_layout.add_widget(self.msg_label)
        layout.add_widget(sub_layout)


        """#################################### Inserting Buttons ####################################"""
        button1 = Button(text="Personal Information",
                         size_hint=(.15, .05),
                         pos_hint={'center_x': .1, 'center_y': .825},
                         on_press = self.on_button_press,
                         background_color= [0,1,0,1])
        layout.add_widget(button1)

        button2 = Button(text="Investment Schemes",
                         size_hint=(.15, .05),
                         pos_hint={'center_x': .25, 'center_y': .825},
                         on_press=self.on_button_press)
        layout.add_widget(button2)

        button3 = Button(text="My Button 3",
                         size_hint=(.15, .05),
                         pos_hint={'center_x': .4, 'center_y': .825},
                         on_press=self.on_button_press)
        layout.add_widget(button3)


        """#################################### Adding Toggled CheckBox ####################################"""
        layout_3 = FloatLayout(size_hint=(.25, .1))
        gender = GenderCheckBox(pos_hint={'center_x': .65, 'center_y': 6})
        layout_3.add_widget(gender)
        layout.add_widget(layout_3)


        """#################################### Adding Slider ####################################"""

        """
        Added a Gridlayout on the parent Floatlayout and positioned & sized as per requirement
        To check the full Gridlayout un-comment the canvas potion in salary_slider class
        """
        layout_2 = FloatLayout(size_hint=(.5, .1))
        sal = salary_slider(pos_hint={'center_x': .5, 'center_y': 7})
        layout_2.add_widget(sal)
        layout.add_widget(layout_2)


        """#################################### Adding Registration Address ####################################"""
        layout_4 = FloatLayout(size_hint=(.45, .8))
        address = TextInputBox(pos_hint={'center_x': .55, 'center_y': .175})
        layout_4.add_widget(address)
        layout.add_widget(layout_4)


        """#################################### Upload Image and display it ####################################"""
        upload = Button(text="Upload Photo",
                        font_size="20sp",
                        size_hint=(.15, .05),
                        pos_hint={'center_x': .9, 'center_y': .825})

        upload.bind(on_release=self.uploadButtonPressed)
        layout.add_widget(upload)


        """#################################### Adding Dropdown ####################################"""
        drop_service = DropdownList()
        layout.add_widget(drop_service)


        """#################################### CLOSE Button ####################################"""
        button_close = Button(text="CLOSE!!!",
                              font_size="25sp",
                              size_hint=(1, .05),
                              pos_hint={'center_x': .5, 'center_y': .05})
        button_close.bind(on_press=lambda x: exit())
        layout.add_widget(button_close)

        """#################################### Rights Notice ####################################"""
        layout.add_widget(Label(text="The application is developed and owned by SIDHANT PATI."
                                     " Any usage has to be approved by the owner. All rights reserved.",
                                font_size="12sp",
                                pos_hint={'center_x': .5, 'center_y': .01}))


        self.add_widget(layout)


    def on_button_press(self, instance):
        # instance captures the value of the button pressed
        if instance.text =="Personal Information":
            self.msg_label.text = instance.text
        elif instance.text == "Investment Schemes":
            self.manager.current = "invest"
        elif instance.text == "My Button 3":
            self.msg_label.text = instance.text

    def on_signout(self, instance):
        self.manager.current = "login"


    def uploadButtonPressed(self, instance):
        """
        The submit button in the TextInputbox is accessed to put a popup to mark the successful updation
        of all values with and to dismiss the popup.
        :param instance:
        :return:
        """
        self.required_content = imageFilechooser()
        self.popup = Popup(title="Select file",
                           content=self.required_content,
                           size_hint=(0.7, 0.7),
                           pos_hint={'center_x': .5, 'center_y': .5})
        self.popup.open()
        self.required_content.file_name.bind(on_press=self.popup.dismiss)
        self.required_content.file_select.bind(on_release=self.popup.dismiss)


class Investment(Screen):
    def __init__(self, **kwargs):
        super(Investment, self).__init__(**kwargs)

        self.layout = layout = BackLayout()

        """#################################### Left Label (User Info) ####################################"""

        head = Header(size_hint=(1, .1), pos_hint={'center_x': .5, 'center_y': .925})
        head.signout.bind(on_release=self.on_signout)
        layout.add_widget(head)

        """#################################### Inserting Buttons ####################################"""
        button1 = Button(text="Personal Information",
                         size_hint=(.15, .05),
                         pos_hint={'center_x': .1, 'center_y': .825},
                         on_press = self.on_button_press)
        layout.add_widget(button1)

        button2 = Button(text="Investment Schemes",
                         size_hint=(.15, .05),
                         pos_hint={'center_x': .25, 'center_y': .825},
                         on_press = self.on_button_press,
                         background_color= [0,1,0,1])
        layout.add_widget(button2)

        button3 = Button(text="My Button 3",
                         size_hint=(.15, .05),
                         pos_hint={'center_x': .4, 'center_y': .825},
                         on_press = self.on_button_press)
        layout.add_widget(button3)

        """#################################### Investment Options ####################################"""

        invest = InvestmentOptions(size_hint=(.45, .6), pos_hint={'center_x': .25, 'center_y': .425})
        layout.add_widget(invest)

        invest_info = InvestmentInfo(size_hint=(.45, .6), pos_hint={'center_x': .75, 'center_y': .425})
        layout.add_widget(invest_info)

        """#################################### CLOSE Button ####################################"""
        button_close = Button(text="CLOSE!!!",
                              font_size="25sp",
                              size_hint=(1, .05),
                              pos_hint={'center_x': .5, 'center_y': .05})
        button_close.bind(on_press=lambda x: exit())
        layout.add_widget(button_close)

        """#################################### Rights Notice ####################################"""
        layout.add_widget(Label(text="The application is developed and owned by SIDHANT PATI."
                                     " Any usage has to be approved by the owner. All rights reserved.",
                                font_size="12sp",
                                pos_hint={'center_x': .5, 'center_y': .01}))

        self.add_widget(self.layout)

    def on_button_press(self, instance):
        # instance captures the value of the button pressed
        if instance.text =="Personal Information":
            self.manager.current = "main"
        elif instance.text == "Investment Schemes":
            pass
        elif instance.text == "My Button 3":
            pass

    def on_signout(self, instance):
        self.manager.current = "login"


class ScreenManagement(ScreenManager):
    def __init__(self, **kwargs):
        super(ScreenManagement, self).__init__(**kwargs)


class SidhantPati(App):
    """
    BUILD is the primary function that gets executed.
    All other functions are required to support a particular case within 'build' function.
    """

    # icon = 'ico/fruit.png'
    # title = 'Awesome Fruit Picker'

    # To access the value from slider_salary from the running app for future reference.
    # https://stackoverflow.com/questions/62272041/how-to-get-value-outside-function-using-python-and-kivy
    # For accessing values on running app use,
    # App.get_running_app().root.ANNUAL_INCOME
    ANNUAL_INCOME = 0.0
    SERVICE = ""

    def build(self): # Self is the instance of the class

        self.sm = ScreenManagement(transition=FadeTransition())
        self.sm.add_widget(loginScreen(name="login"))
        self.sm.add_widget(mainScreen(name="main"))
        self.sm.add_widget(Investment(name="invest"))

        return self.sm


if __name__=="__main__":
    app = SidhantPati() # This executes the 'build' function of SidhantPati() class
    app.run()