import requests
from bs4 import BeautifulSoup as bsp
import pandas as pd
from tabulate import tabulate
import numpy as np

def saving_schemes():

    page = requests.get("https://www.paisabazaar.com/saving-schemes/")
    soup = bsp(page.content, 'lxml')

    data = list()

    table_data = soup.find("table")
    for j in table_data.findAll('tr'):
        row_data = j.findAll('td')
        row = [tr.text.strip() for tr in row_data]
        data.append(row)

    table = pd.DataFrame(data[1:], columns=data[0])

    # Print top 5 elements of the dataframe
    print(tabulate(table.head(5), showindex=False, headers=table.columns))
    print("\n")
    print(table.shape)

    return(table)

def Postoffice_savingschemes():
    page = requests.get("https://www.indiapost.gov.in/Financial/pages/content/post-office-saving-schemes.aspx")
    soup = bsp(page.content, 'lxml')

    data = list()

    # Since multiple tables were having the same class, so I first selected the appropriate segment using the id
    # and then within that portion I selected the appropriate table.
    table_data = soup.find(id="tab1").findAll("div", attrs={"class": "li_content"})

    # Since the required table exists at 10th position, so I select the 9 th element of the above list
    table_data = table_data[9].find("table")

    # To extract individual row elements from the html page
    for j in table_data.findAll('tr'):
        row_data = j.findAll('td')
        row = [tr.text.strip() for tr in row_data]
        data.append(row)

    head = [th.text.strip() for th in table_data.findAll('th')] # To read the column headers
    table = pd.DataFrame(data[1:], columns=head)

    # Selecting only the first 3 elements of the 3rd column, only to select the interest rates and
    # then type casting the 3rd column to float
    table[head[2]] = table[head[2]].str[:3].astype("float64")

    '''
    Calculate the annual rate of return corresponsing to non-annual frequency
    '''
    def annualReturn(val, fre):
        dataVal = 0.0
        if fre == "Annually":
            dataVal = val
        elif fre == "Quarterly" or fre == "Quarterly and Paid":
            dataVal = (np.power(1 + val/400,4) - 1) * 100
        elif fre == "Monthly" or fre == "Monthly and paid":
            dataVal = (np.power(1 + val/1200,12) - 1) * 100
        else:
            dataVal = 0.0

        return(dataVal)

    table["Annual Return %"] = table.apply(lambda x: annualReturn(x['Rate of interest w.e.f 01.04.2020 to 31.12.2020'], x['Compounding Frequency*']), axis=1)

    # Print top 5 elements of the dataframe
    print(tabulate(table.head(5), showindex=False, headers=table.columns))
    print("\n")
    print(table.shape)

    # table.to_excel(writer,
    #                sheet_name="PostOffice Saving Schemes",
    #                index=False,
    #                header=True)

    table.to_excel("PostOffice Saving Schemes.xlsx",
                   index=False,
                   header=True)

    return ()

def elss():
    """
    While scrapping the 'VALUERESERACHONLINE' website, a typical issue that was arising was,
    the scrapper function was unable to find relevant website elements that existed in 'inspect element'
    while scrapping a website.

    On further checking, the inspected element was missing in the page source,
    this tells us that JavaScript is modifying the page after it loads.
    urllib or requests can't run the JavaScript code. So, you'll have to use Selenium.
    One needs to use explicit wait in order to get the element one is looking for.

    Refer:-
    https://stackoverflow.com/questions/48313615/cant-find-a-div-that-exists-with-inspect-element-while-scraping-a-website
    """

    from selenium import webdriver
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.webdriver.common.by import By
    from selenium.common.exceptions import TimeoutException
    from webdriver_manager.chrome import ChromeDriverManager

    '''
    This statement is executed once initially to setup the chrome driver. Once the Chrome driver is installed,
    then we pass the path of the chrome driver for further execution. We can choose to launch the test on Chrome,
    Firefox etc.
    
    Refer:-
    https://stackoverflow.com/questions/29858752/error-message-chromedriver-executable-needs-to-be-available-in-the-path
    '''

    ####################################### 1. For SNAPSHOT of Fundhouses #######################################
    #driver = webdriver.Chrome(ChromeDriverManager().install())
    driver = webdriver.Chrome(executable_path="/Users/sidrocks/.wdm/drivers/chromedriver/mac64/90.0.4430.24/chromedriver")

    '''
    1st Statement - link for all fund houses
    2nd Statement - link for Top Big 15 fund houses
    '''
    #driver.get("https://www.valueresearchonline.com/funds/selector/category/106/equity-elss/?tab=snapshot")
    driver.get("https://www.valueresearchonline.com/funds/selector/category/106/equity-elss/?fund-house=4%2C8799%2C181%2C339%2C28%2C302%2C14%2C298%2C218%2C185%2C327%2C24%2C25%2C27%2C311&tab=snapshot")

    try:
        WebDriverWait(driver, 10)\
            .until(EC.presence_of_element_located((By.CLASS_NAME, 'row-border dataTable table-snapshot no-footer')))
    except TimeoutException:
        pass
        #print('\nPage timed out after 2 secs.\n')

    soup = bsp(driver.page_source, 'lxml')
    driver.quit()

    data = list()

    '''
    The html is arranged in a format which makes it difficult to explicitly capture the required elements
    and hence in order to capture the table data, I explicitly searched for tbody, which had a singular existence.
    '''
    table_data = soup.find("tbody")

    # To extract individual row elements from the html page
    for j in table_data.findAll('tr'):
        row_data = j.findAll('td')
        row = [tr.text.strip() for tr in row_data]
        data.append(row)

    head = [th.text.strip() for th in soup.find("table").findAll('th')] # To read the column headers

    table = pd.DataFrame(data, columns=head)

    print("Extraction of Equity Funds SNAPSHOT completed.")

    ###################################### 2. For LONGTERM RETURN of Fundhouses ######################################
    # driver = webdriver.Chrome(ChromeDriverManager().install())
    driver = webdriver.Chrome(executable_path="/Users/sidrocks/.wdm/drivers/chromedriver/mac64/90.0.4430.24/chromedriver")

    #driver.get("https://www.valueresearchonline.com/funds/selector/category/106/equity-elss/?tab=returns-long-term")
    driver.get("https://www.valueresearchonline.com/funds/selector/category/106/equity-elss/?fund-house=4%2C8799%2C181%2C339%2C28%2C302%2C14%2C298%2C218%2C185%2C327%2C24%2C25%2C27%2C311&tab=returns-long-term")

    try:
        WebDriverWait(driver, 10) \
            .until(EC.presence_of_element_located((By.CLASS_NAME, 'row-border dataTable table-snapshot no-footer')))
    except TimeoutException:
        pass
        # print('\nPage timed out after 2 secs.\n')

    soup = bsp(driver.page_source, 'lxml')
    driver.quit()

    data = list()

    '''
    The html is arranged in a format which makes it difficult to explicitly capture the required elements
    and hence in order to capture the table data, I explicitly searched for tbody, which had a singular existence.
    '''
    table_data = soup.find("tbody")

    # To extract individual row elements from the html page
    for j in table_data.findAll('tr'):
        row_data = j.findAll('td')
        row = [tr.text.strip() for tr in row_data]
        data.append(row)

    head = [th.text.strip() for th in soup.find("table").findAll('th')]  # To read the column headers

    table_longterm = pd.DataFrame(data, columns=head)

    print("Extraction of Equity Finds RETURNS completed.\n")

    table = table.merge(table_longterm, how="left", on="Fund Name") # Merge both tables to accumulate all columns
    table = table.replace("--",np.nan)
    table['Net Assets (Cr)'] = table['Net Assets (Cr)'].str.replace(",", "")

    # Selecting relevant columns and typecasting them to float
    table = table[["Fund Name", "Net Assets (Cr)", "Expense Ratio (%)", "1 Yr Ret (%)", "3 Yr Ret (%)",
                   "5 Yr Ret (%)", "10 Yr Ret (%)", "15 Yr Ret (%)", "20 Yr Ret (%)"]]
    table = table.astype({"Net Assets (Cr)": "int64",
                          "Expense Ratio (%)": "float64",
                          "1 Yr Ret (%)": "float64",
                          "3 Yr Ret (%)": "float64",
                          "5 Yr Ret (%)": "float64",
                          "10 Yr Ret (%)": "float64",
                          "15 Yr Ret (%)": "float64",
                          "20 Yr Ret (%)": "float64"})


    # Print top 5 elements of the dataframe
    print(tabulate(table.head(5), showindex=False, headers=table.columns))
    print("\n")
    print(table.shape)

    # table.to_excel(writer,
    #                sheet_name="ELSS Fund Management",
    #                index=False,
    #                header=True)

    table.to_excel("ELSS Fund Management.xlsx",
                   index=False,
                   header=True)

    return ()

def nps():
    print("\nPlease click on the following link to download the NPS DATASET.")
    print("https://www.valueresearchonline.com/downloads/nps-selector-xls/?source_url=%2Fnps%2Fselector-data%2F%3Foutput%3Dhtml-data\n")

    table = pd.read_excel("Output/nps-performance.xls")

    table = table.replace("--", np.nan)
    table['Assets (₹ Cr)'] = table['Assets (₹ Cr)'].str.replace(",", "")

    # Typecasting of relevant columns
    table = table.astype({"Assets (₹ Cr)": "float64"})

    # Print top 5 elements of the dataframe
    print(tabulate(table.head(5), showindex=False, headers=table.columns))
    print("\n")
    print(table.shape)

    # table.to_excel(writer,
    #                sheet_name="NPS Schemes",
    #                index=False,
    #                header=True)

    table.to_excel(writer,
                   sheet_name="NPS Schemes",
                   index=False,
                   header=True)

    return ()

def nbfc_fd_interestrates():
    bajajfin = "https://www.myloancare.in/fixed-deposit/fd-interest-rates/bajaj-finserv"
    pnbhousing = "https://www.myloancare.in/fixed-deposit/fd-interest-rates/pnb-housing-finance"
    icicihome = "https://www.myloancare.in/fixed-deposit/fd-interest-rates/icici-home-finance"
    hdfc = "https://www.myloancare.in/fixed-deposit/fd-interest-rates/hdfc"
    lichousing = "https://www.myloancare.in/fixed-deposit/fd-interest-rates/lic-housing-finance"

    page_bajaj = requests.get(bajajfin).text
    page_pnb = requests.get(pnbhousing).text
    page_icici = requests.get(icicihome).text
    page_hdfc = requests.get(hdfc).text
    page_lic = requests.get(lichousing).text

    # ------------------------------------------ (Bajaj Finserv) ------------------------------------------

    table_bajaj = pd.read_html(page_bajaj, flavor="bs4", attrs={"class": "table table-striped mt-1"})[-1]
    table_bajaj["Interest Rate"] = table_bajaj["Interest Rate"].str[:-1].astype("float64") # remove '%' & convert float
    table_bajaj["Tenure"] = table_bajaj["Tenure"].str[:3].astype("int64")/12
    table_bajaj["Tenure"] = table_bajaj["Tenure"].astype("int64").astype("string") + " Years"
    table_bajaj = table_bajaj.transpose().reset_index(drop=True)

    header = table_bajaj.iloc[0]
    table_bajaj = table_bajaj[1:]
    table_bajaj.columns = header
    table_bajaj = table_bajaj.reset_index(drop=True)

    # print(tabulate(table_bajaj, showindex=False, headers=table_bajaj.columns))
    # print("\n")

    # ------------------------------------------ (PNB) ------------------------------------------

    table_pnb = pd.read_html(page_pnb, flavor="bs4", attrs={"class": "table table-striped mt-1"})[-1]
    table_pnb["Interest Rate"] = table_pnb["Interest Rate"].str[:-1].astype("float64") # remove '%' & convert float
    table_pnb["Tenure"] = table_pnb["Tenure"].str[:3].astype("int64") / 12
    table_pnb["Tenure"] = table_pnb["Tenure"].astype("int64").astype("string") + " Years"
    table_pnb = table_pnb.transpose().reset_index(drop=True)

    header = table_pnb.iloc[0]
    table_pnb = table_pnb[1:]
    table_pnb.columns = header
    table_pnb = table_pnb.reset_index(drop=True)

    # print(tabulate(table_pnb, showindex=False, headers=table_pnb.columns))
    # print("\n")

    # ------------------------------------------ (ICICI) ------------------------------------------

    table_icici = pd.read_html(page_icici, flavor="bs4", attrs={"class": "table table-striped mt-1"})[-1]
    table_icici["Interest Rate"] = table_icici["Interest Rate"].str[:-1].astype("float64") # remove '%' & convert float
    table_icici["Tenure"] = table_icici["Tenure"].str[:3].astype("int64") / 12
    table_icici["Tenure"] = table_icici["Tenure"].astype("int64").astype("string") + " Years"
    table_icici = table_icici.transpose().reset_index(drop=True)

    header = table_icici.iloc[0]
    table_icici = table_icici[1:]
    table_icici.columns = header
    table_icici = table_icici.reset_index(drop=True)

    # print(tabulate(table_icici, showindex=False, headers=table_icici.columns))
    # print("\n")

    # ------------------------------------------ (HDFC) ------------------------------------------

    table_hdfc = pd.read_html(page_hdfc, flavor="bs4", attrs={"class": "table table-striped mt-1"})[-1]
    table_hdfc["Interest Rate"] = table_hdfc["Interest Rate"].str[:-1].astype("float64") # remove '%' & convert float
    table_hdfc["Tenure"] = table_hdfc["Tenure"].str[:3].astype("int64") / 12
    table_hdfc["Tenure"] = table_hdfc["Tenure"].astype("int64").astype("string") + " Years"
    table_hdfc = table_hdfc.transpose().reset_index(drop=True)

    header = table_hdfc.iloc[0]
    table_hdfc = table_hdfc[1:]
    table_hdfc.columns = header
    table_hdfc = table_hdfc.reset_index(drop=True)

    # print(tabulate(table_hdfc, showindex=False, headers=table_hdfc.columns))
    # print("\n")

    # ------------------------------------------ (LIC) ------------------------------------------

    table_lic = pd.read_html(page_lic, flavor="bs4", attrs={"class": "table table-striped mt-1"}, skiprows=[1])[-1]
    table_lic["Interest Rate"] = table_lic["Interest Rate"].str[:-1].astype("float64") # remove '%' & convert float
    table_lic["Tenure"] = table_lic["Tenure"].str[:2]
    table_lic["Tenure"] = table_lic["Tenure"].astype("int64").astype("string") + " Years"
    table_lic = table_lic.transpose().reset_index(drop=True)

    header = table_lic.iloc[0]
    table_lic = table_lic[1:]
    table_lic.columns = header
    table_lic = table_lic.reset_index(drop=True)

    # print(tabulate(table_lic, showindex=False, headers=table_lic.columns))
    # print("\n")

    # --------------------------------------- (Combining all 5 dataframes) ---------------------------------------

    # Concatinating all dataframes and adding a new column to inset the names of NBFCs
    table = pd.concat((table_bajaj, table_pnb, table_icici, table_hdfc, table_lic), ignore_index=True)
    table["INSTITUTION"] = ["Bajaj Finserv", "PNB Housing Finance", "ICICI Home Finance", "HDFC", "LIC Housing Finance"]

    table = table[['INSTITUTION', '1 Years', '2 Years', '3 Years', '4 Years',
                   '5 Years', '6 Years', '10 Years', '8 Years']]

    print(tabulate(table.head(5), showindex=False, headers=table.columns))

    table.to_excel("NBFC FD Rates.xlsx",
                   index=False,
                   header=True)

    return()

def bank_fd_interestrates():
    """
    Understanding the dynamics of scraping a dynamic responsive table

    Refer:- https://stackoverflow.com/questions/67451433/unable-to-web-scrape-a-dynamic-responsive-table-using-python-selenium-using-opti/67451627#67451627


    url = "https://www.moneycontrol.com/fixed-income/bank_fd_main.php"

    query = {
        "sel_banktype": "Cooperative Banks",
        "sel_amt": 1,
        "sel_cat": "G",
        "sel_period": 8,
        "sel_int": 1,
        "tax_status": "",
        "def_sort": "interest",
        "post_flag": True,
        "call_type": "best"
    }

    '''
    In order to send input in the webpage, first we need to send a POST request along with the required
    parameters as shown in "query". In order to find the required parameters, we can "print(page)" and thereafter
    search for method="post", which shall highlight all the parameters along with their default values.

    Thereafter the "values" for each of the parameters is "value" corresponding to available options.
    eg:-
    "sel_banktype": ""  ----> corresponds to "All"
    "sel_cat": "G"  ----> corresponds to "General"
    "sel_period": 8  ----> corresponds to "2 < 3 yrs"
    "sel_amt": 1  ----> corresponds to "All"

    Also, if don't enable "post_flag", only 1 parameter ("sel_banktype") is getting updated. This is the only
    xpath visible under normal circumstances and hence the change. But if we want to successfully post the
    query updates, then we must enable "post_flag = True" as shown above.
    '''
    page = requests.post(url, data=query).text
    print(page)

    # //Default Output of page
    #
    # < form name = "data_frm" id = "data_frm" method = "post" >
    # < input type = "hidden" name = "sel_banktype" id = "sel_banktype" value = "" >
    # < input type = "hidden" name = "sel_amt" id = "sel_amt" value = "1" >
    # < input type = "hidden" name = "sel_cat" id = "sel_cat" value = "G" >
    # < input type = "hidden" name = "sel_period" id = "sel_period" value = "7" >
    # < input type = "hidden" name = "sel_int" id = "sel_int" value = "" >
    # < input type = "hidden" name = "tax_status" id = "tax_status" value = "N" >
    # < input type = "hidden" name = "def_sort"id = "def_sort" value = "interest" >
    # < input type = "hidden" name = "call_type" id = "call_type" value = "best" >
    # < input type = "hidden" name = "post_flag" id = "post_flag" value = "true" >

    # //Output of page with data
    #
    # < form name = "data_frm" id = "data_frm" method = "post" >
    # < input type = "hidden" name = "sel_banktype" id = "sel_banktype" value = "Cooperative Banks" >
    # < input type = "hidden" name = "sel_amt" id = "sel_amt" value = "1" >
    # < input type = "hidden" name = "sel_cat" id = "sel_cat" value = "G" >
    # < input type = "hidden" name = "sel_period" id = "sel_period" value = "8" >
    # < input type = "hidden" name = "sel_int" id = "sel_int" value = "1" >
    # < input type = "hidden" name = "tax_status" id = "tax_status" value = " >
    # < input type = "hidden" name = "def_sort"id = "def_sort" value = "interest" >
    # < input type = "hidden" name = "call_type" id = "call_type" value = "best" >
    # < input type = "hidden" name = "post_flag" id = "post_flag" value = "true" >

    '''
    pd.read_html() returns a list of dataframes, with the required dataframe being the last one.
    Hence to access the required dataframe we read the [-1] element from the list of dataframes.
    '''
    table = pd.read_html(page, flavor="bs4", skiprows=[0, 2])[-1]
    # skiprows skips rows with index no. as 0 and 2.

    print(tabulate(table, showindex= False))

    '''
    To read the first row of the dataframe as its row.
    Refer:- https://stackoverflow.com/questions/31328861/python-pandas-replacing-header-with-top-row

    And ignore the last column which is null & Calculate
    '''
    header = table.iloc[0]
    table = table[1:]
    table.columns = header
    table = table.loc[:, table.columns.notnull()] # Drop the last column with header as nan

    print("\n----------------------------------------------------------------------------------------\n")
    print(tabulate(table, showindex=False, headers=table.columns))
    """

    url = "https://www.moneycontrol.com/fixed-income/bank_fd_main.php"
    period_dict = {"7": "1+ yrs",
                   "8": "2+ yrs",
                   "9": "3+ yrs",
                   "10": "5+ yrs"
                   }
    dict_of_df = {}

    # Run a for loop across all 4 periods to extract the rate of interests offered
    # ------------------------------------------ (Start of FOR LOOP) ------------------------------------------
    for period in list(range(7,11)):
        query = {
            "sel_banktype": "",
            "sel_amt": 1,
            "sel_cat": "G",
            "sel_period": period,
            "sel_int": 1,
            "tax_status": "",
            "def_sort": "interest",
            "post_flag": True,
            "call_type": "best"
        }

        page = requests.post(url, data=query).text
        table = pd.read_html(page, flavor="bs4", skiprows=[0,2])[-1]

        # To convert the first row as the dataframe header
        header = list(table.iloc[0])
        table = table[1:]
        table.columns = header
        table = table.loc[:, table.columns.notnull()] # Drop the last column with header as nan

        table = table.rename(columns={"INTEREST % p.a":"Int Rate % "+period_dict[str(period)],
                                      "period":period_dict[str(period)],
                                      "INVESTMENT (Rs)":"Amount (Rs) "+period_dict[str(period)],
                                      "Investor Type":"Investor "+period_dict[str(period)],
                                      "tax status":"Taxation "+period_dict[str(period)]})

        # Store the dataframes for various periods into a dict
        dict_of_df[str(period)] = table

    # ------------------------------------------ (End of FOR LOOP) ------------------------------------------

    # To outer join the dictionary of dataframes
    # Refer:- https://stackoverflow.com/questions/53935848/how-to-merge-all-data-frames-in-a-dictionary-in-python
    from functools import partial, reduce

    my_reduce = partial(pd.merge, on=["INSTITUTION", "GROUP"], how="outer")
    table = reduce(my_reduce, dict_of_df.values())

    # Typecasting of relevant columns to float.
    table = table.astype({"Int Rate % 1+ yrs": "float64",
                          "Int Rate % 2+ yrs": "float64",
                          "Int Rate % 3+ yrs": "float64",
                          "Int Rate % 5+ yrs": "float64"})

    # Print top 5 elements of the dataframe
    print(tabulate(table.head(5), showindex=False, headers=table.columns))
    print("\n")
    print(table.shape)

    table.to_excel("Bank FD Interest Rates.xlsx",
                   index=False,
                   header=True)

    return ()

def bank_rd_interestrates():
    url = "https://www.apnaplan.com/highest-interest-rate-on-recurring-deposits-rd/"

    page = requests.get(url).text
    table = pd.read_html(page, flavor="bs4")[-1]
    table = table.replace("x", np.nan)
    table['1 Year'] = table['1 Year'].str.replace("%", "")
    table['2 Years'] = table['2 Years'].str.replace("%", "")
    table['3 years'] = table['3 years'].str.replace("%", "")
    table['4 Years'] = table['4 Years'].str.replace("%", "")
    table['5 Years'] = table['5 Years'].str.replace("%", "")
    table['5+ years'] = table['5+ years'].str.replace("%", "")

    # Typecasting of relevant columns to float.
    table = table.astype({"1 Year": "float64",
                          "2 Years": "float64",
                          "3 years": "float64",
                          "4 Years": "float64",
                          "5 Years": "float64",
                          "5+ years": "float64"})

    # Print top 5 elements of the dataframe
    print(tabulate(table.head(5), showindex=False, headers=table.columns))
    print("\n")
    print(table.shape)

    table.to_excel("Bank RD Interest Rates.xlsx",
                   index=False,
                   header=True)

    return()

#This weiter method is used to write multiple dataframes into the Excel Workbook
# writer = pd.ExcelWriter("/Users/sidrocks/Desktop/MyProj/WebScrapping/Output/Savings Schemes.xlsx", engine='xlsxwriter')

# saving_schemes()
# print("\n--------------------------------- (Savings Schemes Extracted) ------------------------------\n")
# Postoffice_savingschemes()
# print("\n--------------------------------- (PostOffice Rates Extracted) -----------------------------\n")
# elss()
# print("\n--------------------------------- (ELSS Schemes Extracted) ---------------------------------\n")
# nps()
# print("\n--------------------------------- (NPS Schemes Extracted) ---------------------------------\n")
# nbfc_fd_interestrates()
# print("\n--------------------------------- (NBFC FD Rates Extracted) --------------------------------\n")
# bank_fd_interestrates()
# print("\n--------------------------------- (BANK FD Rates Extracted) ---------------------------------\n")
# bank_rd_interestrates()
# print("\n--------------------------------- (BANK RD Rates Extracted) ---------------------------------\n")

# writer.save()