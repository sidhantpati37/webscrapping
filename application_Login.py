from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.graphics import *
from kivy.uix.slider import Slider
from kivy.uix.dropdown import DropDown
from kivy.uix.checkbox import CheckBox
from kivy.uix.textinput import TextInput
from kivy.uix.filechooser import FileChooserIconView, FileChooserListView
from kivy.uix.popup import Popup
import numpy as np


class BackLayout(FloatLayout):
    """
    The class helps to create the background layout of the main screen.
    Creates a Red rectangular Floatlayout, which automatically re-sizes
    and re-positions itself based on screen size.
    """

    def __init__(self, **kwargs):
        super(BackLayout, self).__init__(**kwargs) # **kwargs to inherit various parameters of Floatlayout

        # canvas.before helps to insert colour around the rectangular background
        # and update it based on screen size.
        with self.canvas.before:
            Color(1, 0, 0, .1, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle

        # Event binding refers to activating triggers based on execution of some event
        # As in this case, as the size of window increases or decreases the red background
        # rectangle will undergo change in position and size to fit the window
        self.bind(size=self._update_rect, pos=self._update_rect)

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


class Login(BoxLayout):
    def __init__(self, **kwargs):
        super(Login, self).__init__(**kwargs)

        with self.canvas.before:
            Color(1, 0, 0, .3, mode="rgba")  # Add a red color
            self.rect = Rectangle(pos=self.pos, size=self.size)  # Add a rectangle
        self.bind(size=self._update_rect, pos=self._update_rect)

        self.orientation = "vertical"
        self.padding = [10, 5]

        self.add_widget(Label(text="Sign-In", font_size="30sp"))
        self.add_widget(Label(text="Username", size_hint_x=0))
        self.username = TextInput(text="", multiline=False)
        self.add_widget(self.username)
        self.add_widget(Label(text="Password", size_hint_x=0))
        self.password = TextInput(text="", multiline=False, password=True)
        self.add_widget(self.password)
        self.add_widget(Label(text=""))
        self.continue_button = Button(text="Continue", font_size="20sp")
        self.add_widget(self.continue_button)

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


class SidhantPati(App):
    """
    BUILD is the primary function that gets executed.
    All other functions are required to support a particular case within 'build' function.
    """

    # icon = 'ico/fruit.png'
    # title = 'Awesome Fruit Picker'

    def build(self):
        self.layout = layout = BackLayout()

        self.log = Login(size_hint=(.25, .4), pos_hint={'center_x': .5, 'center_y': .5})
        self.log.continue_button.bind(on_press=self.signin)
        layout.add_widget(self.log)

        layout.add_widget(Label(text="The application is developed and owned by SIDHANT PATI."
                                     " Any usage has to be approved by the owner.",
                                pos_hint={'center_x': .5, 'center_y': .05}))

        return layout

    def signin(self, instance):
        print(self.log.username.text)
        print(self.log.password.text)


if __name__ == "__main__":
    app = SidhantPati()  # This executes the 'build' function of SidhantPati() class
    app.run()